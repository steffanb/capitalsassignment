//
//  main.m
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
