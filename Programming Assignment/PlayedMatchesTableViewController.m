//
//  PlayedMatchesTableViewController.m
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import "PlayedMatchesTableViewController.h"
#import <AFHTTPRequestOperation.h>
#import <AFHTTPRequestOperationManager.h>

@interface PlayedMatchesTableViewController ()


@end

@implementation PlayedMatchesTableViewController

-(id)init{
    
    self = [super init];
    if ( self != nil ) {
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    
    [self loadMatchData];

}

- (void)handleRefresh:(id)sender {
    [self loadMatchData];


    // End Refreshing
    [(UIRefreshControl *)sender endRefreshing];
}

- (void)loadMatchData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *serializer = [[AFJSONRequestSerializer alloc] init];

    [serializer setValue:@"Token 424b977572b5b148e8929389caf8a87f86f43ad4" forHTTPHeaderField:@"Authorization"];
    [manager setRequestSerializer:serializer];
    
    NSDictionary *params = @{@"status" : @"X"};
    
    [manager GET:@"http://cvhj-dev.elasticbeanstalk.com:80/api/matches/"
      parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
            self.matchData = [[ [[NSArray alloc] initWithArray:responseObject] reverseObjectEnumerator] allObjects];
             
           [self.tableView reloadData];
           [self.tableView layoutIfNeeded];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"error code %lu",[operation.response statusCode]);
             switch ([operation.response statusCode]) {
                 case 400:
                    self.errorMessage = @"Bad request";
                    break;
                 case 401:
                    self.errorMessage = @"Unauthorized request";
                    break;
                 case 500:
                    self.errorMessage = @"Internal server error";
                    break;
                 case 502:
                    self.errorMessage = @"Bad gateway";
                    break;
                 default:
                    self.errorMessage = @"Unknown error";
                    break;
             }
             
             UIAlertController *alertController = [UIAlertController
                                                   alertControllerWithTitle:@"Whoops!"
                                                   message:self.errorMessage
                                                   preferredStyle:UIAlertControllerStyleAlert];

             UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
                                        {
                                            NSLog(@"OK action");
                                        }];
            
             [alertController addAction:okAction];
             
             [self presentViewController:alertController animated:YES completion:nil];
             
         }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.matchData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Title"];

    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:@"Title"];
    }
    
    self.format = [[NSDateFormatter alloc] init];
    [self.format setTimeZone:[NSTimeZone timeZoneWithName:@"GMT+02:00"]];
    
    [self.format setDateFormat:@"EEEE dd MMM yyyy"];
    
    self.matchDate = [NSDate dateWithTimeIntervalSince1970: [self.matchData[indexPath.row][@"start_date"] doubleValue ]];
    
    NSString *dateString = [self.format stringFromDate:self.matchDate];

    if(self.matchData[indexPath.row][@"home_score"] > self.matchData[indexPath.row][@"away_score"]){
        self.result = @"home_wins";
    }else if(self.matchData[indexPath.row][@"home_score"] == self.matchData[indexPath.row][@"away_score"]){
        self.result = @"tie";
    }else{
        self.result = @"away_wins";
    }
    
    //NSLog(@"%@", self.matchData[indexPath.row]);

    NSString *homeTeam     = self.matchData[indexPath.row][@"home_team"][@"fullname"];
    NSString *awayTeam = self.matchData[indexPath.row][@"away_team"][@"fullname"];
    
    NSString *titleString = [NSString stringWithFormat:@"%@ - %@", homeTeam, awayTeam];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:titleString];

    
    if([self.result  isEqual: @"home_wins"]){
        
        [string addAttribute:NSForegroundColorAttributeName
                       value:[UIColor colorWithRed:0.00 green:0.77 blue:0.02 alpha:1.0]
                       range:NSMakeRange(0, [homeTeam length])];
         NSString *imageName = [NSString stringWithFormat:@"%@.png", self.matchData[indexPath.row][@"home_team"][@"codename"]];
        UIImageView *iconImage = [[UIImageView alloc] init];
        iconImage.contentMode = UIViewContentModeScaleAspectFit;
        iconImage.frame = CGRectMake(200, 4, 35, 35);
        iconImage.image = [UIImage imageNamed: imageName ];
        
        cell.accessoryView = iconImage;
    }else if([self.result  isEqual: @"away_wins"]){
        
        [string addAttribute:NSForegroundColorAttributeName
                       value:[UIColor colorWithRed:0.00 green:0.77 blue:0.02 alpha:1.0]
                       range:NSMakeRange( ([homeTeam length] + 3), [awayTeam length])];
        
        
        NSLog(@"%@", self.matchData[indexPath.row][@"home_team"]);
        
        NSString *imageName = [NSString stringWithFormat:@"%@.png", self.matchData[indexPath.row][@"away_team"][@"codename"]];
        UIImageView *iconImage = [[UIImageView alloc] init];
        iconImage.contentMode = UIViewContentModeScaleAspectFit;
        iconImage.frame = CGRectMake(200, 4, 35, 35);
        iconImage.image = [UIImage imageNamed: imageName ];

        cell.accessoryView = iconImage;
    }else{
        cell.accessoryView = nil;
    }

    
    [cell.textLabel setAttributedText: string];
    [cell.detailTextLabel setText:
        [NSString stringWithFormat:@"%@ - %@ (%@)",
            self.matchData[indexPath.row][@"home_score"],
            self.matchData[indexPath.row][@"away_score"],
            dateString
         ]
    ];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
