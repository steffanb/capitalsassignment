//
//  LiveMatchesTableViewController.m
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import "LiveMatchesTableViewController.h"
#import <AFHTTPRequestOperation.h>
#import <AFHTTPRequestOperationManager.h>


@interface LiveMatchesTableViewController ()

@end

@implementation LiveMatchesTableViewController


-(id)init{
    
    self = [super init];
    if ( self != nil ) {
        
    }
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    
    [self loadMatchData];
    
}

- (void)handleRefresh:(id)sender {
    [self loadMatchData];
    
    
    // End Refreshing
    [(UIRefreshControl *)sender endRefreshing];
}

- (void)loadMatchData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *serializer = [[AFJSONRequestSerializer alloc] init];
    
    [serializer setValue:@"Token 424b977572b5b148e8929389caf8a87f86f43ad4" forHTTPHeaderField:@"Authorization"];
    [manager setRequestSerializer:serializer];
    
    NSDictionary *params = @{@"status" : @"S"};
    
    [manager GET:@"http://cvhj-dev.elasticbeanstalk.com:80/api/matches/"
      parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             self.matchData = responseObject;
             
             [self.tableView reloadData];
             [self.tableView layoutIfNeeded];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"error code %lu",[operation.response statusCode]);
             switch ([operation.response statusCode]) {
                 case 400:
                     self.errorMessage = @"Bad request";
                     break;
                 case 401:
                     self.errorMessage = @"Unauthorized request";
                     break;
                 case 500:
                     self.errorMessage = @"Internal server error";
                     break;
                 case 502:
                     self.errorMessage = @"Bad gateway";
                     break;
                 default:
                     self.errorMessage = @"Unknown error";
                     break;
             }
             
             UIAlertController *alertController = [UIAlertController
                                                   alertControllerWithTitle:@"Whoops!"
                                                   message:self.errorMessage
                                                   preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
                                        {
                                            NSLog(@"OK action");
                                        }];
             
             [alertController addAction:okAction];
             
             [self presentViewController:alertController animated:YES completion:nil];
         }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.matchData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Title"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"Title"];
    }
    
    [cell.textLabel setText:
        [NSString stringWithFormat:@"%@ - %@",
            self.matchData[indexPath.row][@"home_team"][@"fullname"],
            self.matchData[indexPath.row][@"away_team"][@"fullname"]
         ]
    ];
    
    [cell.detailTextLabel setText:
        [NSString stringWithFormat:@"%@ - %@",
            self.matchData[indexPath.row][@"home_score"],
            self.matchData[indexPath.row][@"away_score"]
         ]
     ];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
