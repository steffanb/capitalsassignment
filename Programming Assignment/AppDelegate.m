//
//  AppDelegate.m
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import "AppDelegate.h"
#import "PlayedMatchesTableViewController.h"
#import "LiveMatchesTableViewController.h"
#import "FixtureTableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    PlayedMatchesTableViewController *playedMatchesTableViewController = [[PlayedMatchesTableViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *playedMatchesNavController = [[UINavigationController alloc] initWithRootViewController:playedMatchesTableViewController];
    [playedMatchesTableViewController setTitle:@"Played Matches"];
    
    
    LiveMatchesTableViewController *liveMatchesTableViewController = [[LiveMatchesTableViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *liveMatchesNavController = [[UINavigationController alloc] initWithRootViewController:liveMatchesTableViewController];
    [liveMatchesTableViewController setTitle:@"Live Matches"];
    
    FixtureTableViewController *fixtureTableViewController = [[FixtureTableViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *fixtureNavController = [[UINavigationController alloc] initWithRootViewController:fixtureTableViewController];
    [fixtureTableViewController setTitle:@"Upcoming Matches"];
    
    
    NSArray *tabArray = @[playedMatchesNavController, liveMatchesNavController, fixtureNavController];
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    [tabBarController setViewControllers:tabArray];
    

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
