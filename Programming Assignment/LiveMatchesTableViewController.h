//
//  LiveMatchesTableViewController.h
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveMatchesTableViewController : UITableViewController

- (void)handleRefresh:(id)sender;
- (void)loadMatchData;

@property (strong, nonatomic) NSArray *matchData;
@property NSString *errorMessage;

@end
