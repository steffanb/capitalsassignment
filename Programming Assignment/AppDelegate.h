//
//  AppDelegate.h
//  Programming Assignment
//
//  Created by Steffan Bruin on 03/12/15.
//  Copyright © 2015 Steffan Bruin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

